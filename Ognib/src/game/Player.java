package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public abstract class Player {

	int[] board = new int[Ognib.LENGTH]; // represents the board as a 1-D array
	String name;

	public Player(String name) {
		this.name = name;
		initialize();
	}

	private void initialize() {
		Random rand = new Random();
		int randomNumber; // an integer to be chosen at random
		boolean isDuplicate; // to indicate is randomNumber has already been picked or not

		// for every cell i in the board
		for (int i = 0; i < board.length; ++i) {
			do { // Generate a random number between MAX_VALUE and MIN_VALUE inclusive
				randomNumber = rand.nextInt((Ognib.MAX_VALUE - Ognib.MIN_VALUE) + 1) + Ognib.MIN_VALUE;

				isDuplicate = false; // initialy, let's assume this is not a duplicate

				// check if this random value has already been inserted in the array in a previous cell
				for (int j = 0; j < i; ++j) {
					if (board[j] == randomNumber) {
						isDuplicate = true;
					}
				}
			} while (isDuplicate); // pick a new random number until we pick a non-duplicate
			board[i] = randomNumber; // this random number is not a duplicate, assign it to the ith cell
		}

		// Sort the array
		Arrays.sort(board);
	}

	public String printArray() {
		String seperator = "===========================";
		String s = seperator + "\n";
		for (int i = 0; i < Ognib.LENGTH; i++) {
			s += "| " + board[i] + " ";
			if ((i + 1) % Ognib.ROWS == 0) {
				s += "|\n";
			}
		}
		s += seperator;
		return s;

	}

	public abstract int receiveChoice();

	public void chose(int number) {

		for (int i = 0; i < board.length; ++i) {
			if (board[i] == number) {
				board[i] = 0;
			}
		}
	}

	public boolean hasWon() {
		List<Integer> zeros = new ArrayList<Integer>();
		// Zero indices
		for (int i = 0; i < Ognib.LENGTH; ++i) {
			if (board[i] == 0) {
				zeros.add(i);
			}
		}
		// Horizontals
		for (int i = 0; i < Ognib.ROWS; ++i) {
			boolean win = true;
			for (int j = 0; j < Ognib.COLUMNS; ++j) {
				if (board[i * Ognib.COLUMNS + j] != 0) {
					win = false;
					break;
				}
			}
			if (win) {
				return true;
			}
		}

		// Verticals
		for (int i = 0; i < Ognib.COLUMNS; ++i) {
			boolean win = true;
			for (int j = 0; j < Ognib.ROWS; ++j) {
				if (board[i * Ognib.ROWS + j] != 0) {
					win = false;
					break;
				}
			}
			if (win) {
				return true;
			}

		}

		// Diagonals (north-west to south-east)
		boolean win = true;
		for (int i = 1; i <= Ognib.ROWS; ++i) {
			if (board[(i * i) - 1] != 0) {
				win = false;
				break;
			}
		}
		if (win) {
			return true;
		}
		// Diagonals (north-east to south-west)
		win = true;
		for (int i = 0; i < Ognib.COLUMNS; i++) {
			if (board[Ognib.COLUMNS * i + Ognib.COLUMNS - i - 1] != 0) {
				win = false;
				break;
			}
		}
		if (win) {
			return true;
		}
		return false;
	}
}
