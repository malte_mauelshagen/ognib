package game;

import java.util.Random;

public class ComputerPlayer extends Player {

	public ComputerPlayer(String name) {
		super(name);
	}

	public int receiveChoice() {
		Random rand = new Random();
		int index = rand.nextInt(Ognib.LENGTH);
		return board[index];
	}

}
