package game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Ognib {

	public static final int ROWS = 5;
	public static final int COLUMNS = 5;
	public static final int LENGTH = ROWS * COLUMNS;
	public static final int MIN_VALUE = 1; // represents the minimum value in the board
	public static final int MAX_VALUE = 100; // represents the maximum value in the board

	public static void main(String[] args) throws IOException {

		Player daniel = new HumanPlayer("Daniel");
		Player computer = new ComputerPlayer("Computer");
		List<Player> players = new ArrayList<Player>();
		players.add(daniel);
		players.add(computer);

		System.out.println("Welcome to Ognib! Let's play.");
		int number;
		while (true) {
			System.out.println(daniel.printArray());
			System.out.println(computer.printArray());
			// 1st computer pick
			number = computer.receiveChoice();
			computer.chose(number);
			daniel.chose(number);
			// 2nd computer pick
			number = computer.receiveChoice();
			computer.chose(number);
			daniel.chose(number);
			// 3rd computer pick
			number = computer.receiveChoice();
			computer.chose(number);
			daniel.chose(number);
			// human pick
			number = daniel.receiveChoice();
			computer.chose(number);
			daniel.chose(number);

			// Check for victory
			for (Player p : players) {
				if (p.hasWon()) {
					System.out.println("Congratulations! " + p.name + " has won!");
					System.out.println(p.printArray());
					System.exit(0);
				}
			}
		}
	}
}
