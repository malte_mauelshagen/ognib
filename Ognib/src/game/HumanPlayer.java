package game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class HumanPlayer extends Player {

	public HumanPlayer(String name) {
		super(name);
	}

	public int receiveChoice() {
		System.out.println(name + "! Please choose a number!");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int i = -1;
		try {
			i = Integer.parseInt(br.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NumberFormatException nfe) {
			System.err.println("Invalid Format!");
		}

		return i;
	}
}
